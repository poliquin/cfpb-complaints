Consumer Financial Protection Bureau - Consumer Complaints
==========================================================

This folder contains data from the [CFPB's Consumer Complaint Database][1]
downloaded on May 13, 2018.

The data were downloaded because [NPR reports][2] that the CFPB may end
online public access to the database.

There are two versions of the data: a CSV file and a JSON file. The files
are compressed with `gzip` and are about 521MB and 708MB respectively when
decompressed.


[1]: https://www.consumerfinance.gov/data-research/consumer-complaints/
[2]: https://www.npr.org/2018/04/25/605835307/the-consumer-complaints-database-that-could-disappear-from-view
